package com.modernjava.funcprogramming;

import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample2 {
    public static void main(String[] args) {
        List<Instructor> instructors = Instructors.getAll();

        //looping through all the instructor and printing out the values of instructor
        Consumer<Instructor> c1 = (s1) -> System.out.println(s1);
        instructors.forEach(c1);
        System.out.println("--------------");

        //loop through all the instructors and only print out their name
        Consumer<Instructor> c2 = (s1) -> System.out.print(s1.getName());
        instructors.forEach(c2);
        System.out.println("--------------");

        //loop through all the instructors and print out their names and their courses
        Consumer<Instructor> c3 = (s1) -> System.out.println(s1.getCourses());
        instructors.forEach(c2.andThen(c3));

        //loop through all the instructors and print out their name depending if the yearsofexp is greater than 10
        System.out.println("---------------");
        instructors.forEach(s1 -> {
            if(s1.yearsOfExperience > 10){
                c1.accept(s1);
            }
        });

        //loop through all the instructors and print put their name and years of experience if YOE
        // is greater 5 and teaches course online
        System.out.println("---------------");
        instructors.forEach(s1 -> {
            if(s1.yearsOfExperience > 5 && !s1.isOnlineCourses()) {
                c1.andThen(c2).accept(s1);
            }
        });
    }
}
