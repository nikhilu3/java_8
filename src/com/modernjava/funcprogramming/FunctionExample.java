package com.modernjava.funcprogramming;

import java.util.Locale;
import java.util.function.Function;

public class FunctionExample {
    public static void main(String[] args) {

        Function<Integer, Double> sqrt = n -> Math.sqrt(n);
        System.out.println("Square root of 64: " + sqrt.apply(64));
        System.out.println("Square root of 81: " + sqrt.apply(81));

        Function<String,String> lowercase = s ->s.toLowerCase();
        System.out.println(lowercase.apply("PROGRAMMING"));

        Function<String,String> concat = s -> s.concat(" In Java");
        System.out.println(lowercase.andThen(concat).apply("PROGRAMMING"));
    }
}
