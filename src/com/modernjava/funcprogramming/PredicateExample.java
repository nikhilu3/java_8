package com.modernjava.funcprogramming;

import java.util.function.Predicate;

public class PredicateExample {
    public static void main(String[] args) {
        //if number is greater than 10 return true
        Predicate<Integer> p1 = (i) -> i > 10;
        System.out.println(p1.test(100));

        // i>10 and number is even
        Predicate<Integer> p2 = (i) -> i%2==0;
        System.out.println(p1.and(p2).test(25));

        // i>10 or number is even
        System.out.println(p1.or(p2).test(25));

        // i>10 && number is odd
        System.out.println(p1.and(p2.negate()).test(33));

    }
}
