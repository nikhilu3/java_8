package com.modernjava.parallelstream;

import org.w3c.dom.ls.LSOutput;

import java.util.function.Supplier;
import java.util.stream.IntStream;

import static java.lang.System.*;

public class StreamPerformance {
    public static void main(String[] args) {
        int loop = 20;
        long result = measurePerformance(StreamPerformance::sumSequential, loop);
        out.println("Time taken to process sum in sequential: " + result + " in ms");
        out.println("---------");
        result = measurePerformance(StreamPerformance::sumParallel, loop);
        out.println("Time taken to process sum in parallel: " + result + " in ms");
    }

    public static long measurePerformance(Supplier<Integer> supplier, int numberOfTimes){
        long startTime = currentTimeMillis();
        for (int i = 0; i < numberOfTimes; i++) {
            supplier.get();
        }
        return currentTimeMillis() - startTime;
    }

    public static int sumSequential(){
        return IntStream.rangeClosed(0,10000000).sum();
    }

    public static int sumParallel(){
        return IntStream.rangeClosed(0,10000000).parallel().sum();
    }
}
