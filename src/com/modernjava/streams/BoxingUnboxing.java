package com.modernjava.streams;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class BoxingUnboxing {
    public static void main(String[] args) {
        List<Integer> numbers;
        IntStream intStream =IntStream.rangeClosed(0,5000); // primitive int stream
        numbers = intStream.boxed().collect(Collectors.toList());
        numbers.forEach(System.out::println);

        Optional<Integer> optional = numbers.stream().reduce(Integer::sum);
        optional.ifPresent(System.out::println);
        System.out.println("--------");
        int sum1 = numbers.stream().mapToInt(Integer::intValue).sum();
        System.out.println(sum1);
    }
}
