package com.modernjava.streams;

import com.modernjava.funcprogramming.Instructor;
import com.modernjava.funcprogramming.Instructors;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupingBy1 {
    public static void main(String[] args) {
        // group list of names by their length

        List<String>names = List.of("Syed", "Mike", "Jenny", "Gene", "Rajeev");
        Map<Integer,List<String>> result = names.stream()
                                                .collect(Collectors.groupingBy(String::length));
        System.out.println(result);

        System.out.println("---------------");
        // grouping by their gender

        Map<String, List<Instructor>> result1 = Instructors.getAll().stream()
                                                .collect(Collectors.groupingBy(Instructor::getGender));
        result1.forEach((key, value) -> System.out.println(key + " -> " +value));

        System.out.println("-----------------");

        //grouping by exp , > 10 years is classified as senior otherwise junior

        Map<String, List<Instructor>> result2 = Instructors.getAll().stream()
                                                            .collect(Collectors.groupingBy(i ->
                                                                    i.getYearsOfExperience() > 10 ? "Senior" : "Junior"));
        result2.forEach((key,value) -> System.out.println(key + " -> " + value));
    }
}
