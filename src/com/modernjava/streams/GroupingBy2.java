package com.modernjava.streams;

import com.modernjava.funcprogramming.Instructor;
import com.modernjava.funcprogramming.Instructors;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class GroupingBy2 {
    public static void main(String[] args) {
        // grouping by length of string and also checking that the names contains e
        // and only those names which has e in it

        List<String> names = List.of("Sid", "Mike", "Jenny", "Gene", "Rajeev");
        Map<Integer, List<String>> result = names.stream()
                                                 .collect(Collectors.groupingBy(String::length,
                                                         Collectors.filtering(s -> s.contains("e"),Collectors.toList())));
        System.out.println(result);

        // instructor grouping them by senior(>10) and junior(<10)
        // and filter them by online courses
        Map<String, List<Instructor>> list = Instructors.getAll().stream()
                                           .collect(Collectors.groupingBy(s -> s.getYearsOfExperience() > 10 ? "Senior"
                                                   : "Junior",
                                                   Collectors.filtering(Instructor::isOnlineCourses,Collectors.toList())));
        System.out.println(list);

        // groupingBy3
        Map<Integer, List<String>> result2 = names.stream()
                .collect(Collectors.groupingBy(String::length, LinkedHashMap::new,
                        Collectors.filtering(s -> s.contains("e"),Collectors.toList())));
        System.out.println(result);
    }
}
