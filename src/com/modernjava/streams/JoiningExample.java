package com.modernjava.streams;

import com.modernjava.funcprogramming.Instructor;
import com.modernjava.funcprogramming.Instructors;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JoiningExample {
    public static void main(String[] args) {
        String s = Stream.of("A","B","C","D").collect(Collectors.joining());
        System.out.println(s);
        System.out.println("---------");
        s = Stream.of("E","F","G","H").collect(Collectors.joining(","));
        System.out.println(s);
        System.out.println("---------");
        s = Stream.of("E","F","G","H").collect(Collectors.joining(",", "{", "}"));
        System.out.println(s);

        // instructors names separated by ' and prefix { and suffix }
        String names = Instructors.getAll().stream()
                       .map(Instructor::getName).collect(Collectors.joining(",","{","}"));
        System.out.println(names);
    }
}
