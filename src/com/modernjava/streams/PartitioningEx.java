package com.modernjava.streams;

import com.modernjava.funcprogramming.Instructor;
import com.modernjava.funcprogramming.Instructors;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PartitioningEx {
    public static void main(String[] args) {
        // first is yoe > 10 and other is <= 10
        Predicate<Instructor> experience = instructor -> instructor.getYearsOfExperience() > 10;
        Map<Boolean, List<Instructor>> partitionMap = Instructors.getAll().stream()
                                                                 .collect(Collectors.partitioningBy(experience));
        partitionMap.forEach((key,value) -> {
            System.out.println("key " + key + " value " + value);
        });

    }
}
