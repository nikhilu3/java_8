package com.modernjava.streams;

import com.modernjava.funcprogramming.Instructors;

import java.util.List;
import java.util.Optional;

public class ReduceExample2 {
    public static void main(String[] args) {
        //names of the instructor who has the highest year of experience

       Optional instructor = Instructors.getAll().stream()
                    .reduce((s1,s2) -> s2.getYearsOfExperience() > s1.getYearsOfExperience()
                            ? s2 : s1);
       if(instructor.isPresent())
        System.out.println(instructor.get());
    }
}
