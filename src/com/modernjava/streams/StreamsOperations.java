package com.modernjava.streams;

import com.modernjava.funcprogramming.Instructor;
import com.modernjava.funcprogramming.Instructors;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsOperations {
    public static void main(String[] args) {
        List<Instructor> list = Instructors.getAll().stream()
                                .distinct()
                                .collect(Collectors.toList());
        System.out.println(list);
        System.out.println("---------------");

        long c = Instructors.getAll().stream()
                 .count();
        System.out.println(c);
        System.out.println("---------------");

       List<String> s = Instructors.getAll().stream()
               .map(Instructor::getCourses)
               .flatMap(List::stream)
               .distinct()
               .sorted().collect(Collectors.toList());
        System.out.println(s);

        // anymatch, allmatch and nonematch

        boolean match = Instructors.getAll().stream()
                     .map(Instructor::getCourses)
                     .flatMap(List::stream)
                     .noneMatch(sk -> sk.startsWith("J") );
        System.out.println(match);
    }
}
